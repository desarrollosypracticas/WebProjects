//Adaptación de adictosaltrabajo.com

$(document).ready( function(){
  
    //Modificado para que funcione
    $("input[type=submit]").click(function () {
    
      // obtenemos el valor del campo de búsqueda, el que contiene el texto de la misma
      var tags = $("input[name=tags]").val();
 
      // hacemos uso del soporte de Ajax que evalua el resultado como un objeto JSON, pasándole como parámetro la url del
      // servicio web de flickr, asignando el texto de la búsqueda. El segundo parámetro de la función es un método de callback
      // que se ejecutará tras la respuesta del servicio y que recibe el objeto JSON en el parámetro data.
      $.getJSON("http://api.openweathermap.org/data/2.5/forecast/city?id=3995402&APPID=4eea18589cf04afd7b1a0a33fc149a8d",
        function(data){
	
          // limpiamos el contenedor del resultado de la búsqueda.
          $("#images").empty();
	  
          // por cada uno de los items que contiene el objeto JSON obtenido invoca a una función que recibe el ordinal y el propio item
          $.each(data.items, function(i,item){
	  
            $("<img/>").attr("src", item.media.m).attr("alt", item.title).attr("title", item.title).appendTo("#images");
	    
            // cortamos el proceso al llegar a 20 imágenes
            if ( i == 20 ) return false;
        }); //fin del each
      
      // asignamos el texto de búsqueda al campo de salida del resultado
      $("#tagsToSearch").text(tags+":00");
    }); //fin del getJSON
   
  
    });//fin del input:submit

}); //fin del ready