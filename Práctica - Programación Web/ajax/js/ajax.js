//Ejemplo de ajax

$(function () {
    //alert("Hola...");
    //$("h1").css("color", "darkblue");
    var hab;
    $("h1").css({"color":"blue", "background-color":"black", "font-size":"4.5em"});
    
    $("input[name=nivel]").change(nivel);
        
});

function nivel(){
    var niv = $(this).val();
        //alert("Valor="+niv);
    switch(niv){
        case 'L':
            $("#adicional").load("licenciatura.html");            
            break;
        case 'M':
            $("#adicional").load("maestria.html");            
            break;
        case 'D':
            $("#adicional").load("doctorado.html");            
            break;    
    }    
    }

function lic(){
    var num = $("#no_de_control").val();
    var pre = $("#prepa").val();
    var pro = $("#prom").val();
    var mensaje = document.getElementById("confirmacion");
    mensaje.innerHTML = "<hr/> <h1>"+"Número de control: "+num+"<br>Nivel: Licenciatura <br> Prepa: "+pre+"<br>Promedio: "+pro+"<h1>";
    
}

function habil(){
    if($("#tes").attr("checked")=="checked"){
        //alert("desHabilitado");
        $("#tes").removeAttr("checked");
        $("#tesis").attr("disabled", "disabled").val("");   
        hab = "No aplica";
    }
    else{
        //alert("habilitado");
        $("#tes").attr("checked", "checked");
        $("#tesis").removeAttr("disabled");          
    }
}

function maest(){
    var num = $("#no_de_control").val();  
    var licenc = $("#lic").val();
    var prom = $("#prom").val();  
    var mensaje = document.getElementById("#confirmacion");
    if($($("#tes").attr("checked")=="checked")){
        hab = $("#tesis").val();        
    }  
    else{
        hab = "No aplica";
    }
    mensaje.innerHTML = "<hr/> <h1>"+"Número de control: "+num+"<br>Nivel: Maestria <br> Licenciatura: "+licenc+"<br>Promedio: "+prom+"<br>Tesis: "+hab+"<h1>";
}


function doc(){
    var num = $("#no_de_control").val();  
    var maestri = $("#mas").val();
    var prom = $("#prom").val();  
    var tesis = $("#tesis").val();
    var mensaje = document.getElementById("confirmacion");    
    mensaje.innerHTML = "<hr/> <h1>"+"Número de control: "+num+"<br>Nivel: Doctorado <br> Maestria: "+maestri+"<br>Promedio: "+prom+"<br>Tesis: "+tesis+"<h1>";
}








