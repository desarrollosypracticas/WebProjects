//Probando jqueryui

var pos=0;

$(function(){
    //alert("hola");
    $("#uno").draggable();
    $("#dos").draggable();
    $("#tres").draggable();
});

function moverNotas(x){
    switch(x){
        case 1:
            pos++;
            $("#area").droppable({
                    drop: function(event,ui){
                        $("#tengo").addClass('ui-highlight').find("#"+pos).html("Caja 1 seleccionada y soltada.");
                    }
                });
            break;
        case 2:
            pos++;
            $("#area").droppable({
                    drop: function(event,ui){
                        $("#tengo").addClass('ui-highlight').find("#"+pos).html("Caja 2 seleccionada y soltada.");
                    }
                });
            break;
        case 3:
            pos++
            $("#area").droppable({
                    drop: function(event,ui){
                        $("#tengo").addClass('ui-highlight').find("#"+pos).html("Caja 3 seleccionada y soltada.");
                    }
                });
            break;            
    }    
}

function nota1(){
    var nota=1;
    moverNotas(nota);
}
function nota2(){
    var nota=2;
    moverNotas(nota);
}
function nota3(){
    var nota=3;
    moverNotas(nota);
}
window.onload=function(){
    var moverNotaUno;
    var moverNotaDos;
    var moverNotaTres;
    moverNota=document.getElementById("uno");
    moverNota.onmouseup=nota1;
    moverNotaDos=document.getElementById("dos");
    moverNotaDos.onmouseup=nota2;
    moverNotaTres=document.getElementById("tres");
    moverNotaTres.onmouseup=nota3;
}