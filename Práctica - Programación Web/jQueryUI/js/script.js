    var lugar=0;
$(document).on('ready',main);
function main(){
    $("#bt1").button();
    $("#dialogo").dialog({
        autoOpen:false
    });
    $("#bt1").click(function(){
        $("#dialogo").dialog('open');
    });
    var cssNota1={
        'position': 'absolute',
        'top': '2%',
        'right': '80%',
        'left': '8%',
        'height': '20%',
        'font-size': '10px',
        'z-index': '1',
        'border': '1px solid purple'
    }
    $("#nota1").css(cssNota1);
    var cssNota2={
        'position': 'absolute',
        'top': '24%',
        'right': '80%',
        'left': '8%',
        'height': '20%',
        'font-size': '10px',
        'z-index': '1',
        'border': '1px solid purple'
    }
    $("#nota2").css(cssNota2);
    var cssNota3={
        'position': 'absolute',
        'top': '46%',
        'right': '80%',
        'left': '8%',
        'height': '20%',
        'font-size': '10px',
        'z-index': '1',
        'border': '1px solid purple'
    }
    $("#nota3").css(cssNota3);
    $("#nota1").draggable();
    $("#nota2").draggable();
    $("#nota3").draggable();
     var cssMensaje={
        'position': 'absolute',
        'top': '2%',
        'right': '39%',
        'left': '31%',
        'height': '64%',
        'border': '1px solid purple',
         'background-color': 'purple'
    }
    $("#mensaje").css(cssMensaje);
    var cssSeleccionado={
        'position': 'absolute',
        'top': '2%',
        'right': '8%',
        'left': '72%',
        'height': '64%',
        'border': '1px solid purple'
    }
    $("#seleccionado").css(cssSeleccionado);
    var cssAcordion={
        'position': 'absolute',
        'top': '68%',
        'right': '8%',
        'left': '8%',
        'height': '20%',
        'font-size': '10px',
    }
    $("#acordion").css(cssAcordion);
    $("#acordion").accordion();
    var cssFormulario={
        'position': 'absolute',
        'top': '98%',
        'right': '8%',
        'left': '8%',
        'height': '100%',
        'font-size': '12px',
    }
    $("#formulario").css(cssFormulario);
    $("#fecha").datepicker();
    $("#edad").spinner({
        min: 0
    });
    $("#volumen").slider();
   $("#menu").menu();
    var cssMenu={
        'position': 'absolute',
        'top': '32%',
        'right': '80%',
        'left': '0%',
        'height': '14%',
    }
    $("#menu").css(cssMenu);
    $("#semestre").selectmenu("option");
    
}
function moverNotas(x){
    switch(x){
        case 1:
            lugar++;
            $("#mensaje").droppable({
                    drop: function(event,ui){
                        $("#seleccionado").addClass('ui-highlight')
                        .find("#"+lugar).html("Nota 1");
                    }
                });
            break;
        case 2:
            lugar++;
            $("#mensaje").droppable({
                    drop: function(event,ui){
                        $("#seleccionado").addClass('ui-highlight')
                        .find("#"+lugar).html("Nota 2");
                    }
                });
            break;
        case 3:
            lugar++;
            $("#mensaje").droppable({
                    drop: function(event,ui){
                        $("#seleccionado").addClass('ui-highlight')
                        .find("#"+lugar).html("Nota 3");
                    }
                });
            break;
            
    }
        //var f=document.getElementById("uno").innerHTML;
        //alert(f);
    
}
function nota1(){
    var nota=1;
    moverNotas(nota);
}
function nota2(){
    var nota=2;
    moverNotas(nota);
}
function nota3(){
    var nota=3;
    moverNotas(nota);
}
window.onload=function(){
    var moverNotaUno;
    var moverNotaDos;
    var moverNotaTres;
    moverNota=document.getElementById("nota1");
    moverNota.onmouseup=nota1;
    moverNotaDos=document.getElementById("nota2");
    moverNotaDos.onmouseup=nota2;
    moverNotaTres=document.getElementById("nota3");
    moverNotaTres.onmouseup=nota3;
}