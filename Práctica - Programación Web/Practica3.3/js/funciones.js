/*Ejemplo de jQuery*/

//$(document).ready(function(){
$(function(){
    
    //alert("hola mundo...");
    console.log("entrando...");
    
    $("#rojo,#verde,#azul,#trans").change(function(){
        console.log("colores");
        
        var rojo = $("#rojo").val();        
        var verde = $("#verde").val();
        var azul = $("#azul").val();
        var trans = $("#trans").val();
        
        rojo = parseInt(rojo, 10);
        verde = parseInt(verde, 10);
        azul = parseInt(azul, 10);
        trans = parseInt(trans, 10);
        
        $("#color").css("background-color", "rgba("+rojo+","+verde+","+azul+","+"0."+trans+")");
        
        rojo=rojo.toString(16);
        verde=verde.toString(16);
        azul=azul.toString(16);
        trans=trans.toString(16);
        
        $("#valor").text(rojo+","+verde+","+azul+","+trans);      
        
        });     
       
    
});