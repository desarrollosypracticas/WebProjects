var flag = false;

$(document).ready(function() {
    $("#contenido").submit(function(event) {
        event.preventDefault();
        doStuff();
    });
    $("#redsocial").click(function() {
        //$("#triggerbtn").trigger("click");
        $("#triggerbtn").click();
        /*$("#datosgenerales").submit(function() {
            
            return false;
        });*/
    });
    $("#redsocial").change(function() {
        switch($(this).val()) {
            case "1":
                $("#infored").load("assets/facebook.html");
                break;
            case "2":
                $("#infored").load("assets/twitter.html");
                break;
            case "3":
                $("#infored").load("assets/instagram.html");
                break;
        }
    });
});

function doStuff() {
    if( flag == false ) {
        $("#datosgenerales").show(1000);
        $("#infored").show(1000);
        $("#enviar").val("Ocultar Datos Generales");
        flag = true;
    } else {
        $("#datosgenerales").hide(1000);
        $("#infored").hide(1000);
        $("#enviar").val("Agregar Datos Generales");
        flag = false;
    }
}