<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ponys ITMorelia</title>
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="shortcut" href="img/icon.png">
    <!--Script de javascript para el carrusel-->
</head>
<body>
    
    <!--<header class="main-header">

  <h1 class="logo header-chunk">

  <a href="http://codepen.io/" class="small-screen-logo">
    <svg class="icon-codepen-box">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#codepen-box"></use>
    </svg>
  </a>

  <a href="http://codepen.io/" class="large-screen-logo">
    CodePen
  </a>

</h1>

  <nav class="header-chunk primary-nav fill-space priority-nav show-more-text" id="secondary-primary-nav" style="overflow: visible;">
    <ul style="white-space: nowrap;">
      <li class="primary primary-pens hide" id="primary-nav-pens"><a href="/pens/">Pens</a></li>
      <li class="primary primary-posts hide"><a href="/posts/">Posts</a></li>
      <li class="primary primary-collections primary-last hide"><a href="/collections/">Collections</a></li>
      <li class="primary-spark hide"><a href="/spark/">Spark</a></li>
      <li class="primary-jobs hide"><a href="/jobs/">Jobs</a></li>
      <li class="primary-blog hide"><a href="http://blog.codepen.io/">Blog</a></li>
      <li class="primary-store hide"><a href="http://blog.codepen.io/store/">Store</a></li>
        <li class="primary-pro hide"><a href="/pro/">Go <span class="badge badge-pro">PRO</span></a></li>
      <li class="overflow-nav show-inline-block">
        <span id="overflow-nav-menu-link">
          <span class="primary-nav-more">Menu</span>
          <span class="overflow-nav-title">
            <svg class="dropdown-arrow">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-down-mini"></use>
            </svg>
          </span>
          <ul class="overflow-nav-list link-list priority-nav__dropdown open" style="white-space: nowrap;"><li class="primary primary-pens" id="primary-nav-pens"><a href="/pens/">Pens</a></li><li class="primary primary-posts"><a href="/posts/">Posts</a></li><li class="primary primary-collections primary-last"><a href="/collections/">Collections</a></li><li class="primary-spark"><a href="/spark/">Spark</a></li><li class="primary-jobs"><a href="/jobs/">Jobs</a></li><li class="primary-blog"><a href="http://blog.codepen.io/">Blog</a></li><li class="primary-store"><a href="http://blog.codepen.io/store/">Store</a></li><li class="primary-pro"><a href="/pro/">Go <span class="badge badge-pro">PRO</span></a></li></ul>
        </span>
      </li>
    </ul>
  </nav>

    <div class="header-chunk primary-actions old-header-buttons">
      <div class="multi-button">
        <a href="http://codepen.io/pen/" class="button button-medium new-pen-button">
          <span class="icon">
            +
          </span>
          New Pen
        </a>
      </div>

    </div>


  <div class="pen-templates-dropdown is-dropdown link-list" data-dropdown-position="css" id="pen-templates-dropdown"></div>

  
  <div id="user-header-dropdown" class="user-stuff header-chunk">

    <a id="login-button" class="button button-medium login-button" href="https://codepen.io/login">Log In</a>

    <a href="/signup/" class="button button-medium signup-button">Sign Up</a>

  </div>




    <div class="header-search header-chunk" id="header-search" role="search">

  <a href="#0" class="header-search-button" id="header-search-button">
    <svg class="header-icon-mag" width="25px" height="65px">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#mag"></use>
    </svg>
  </a>

</div>



</header>-->
    
    <!--Inicio de carrusel-->
   <div>
       <section class="galeria">
           <input type="radio" id="uno" value="1" name="tractor" checked="checked">
           <input type="radio" id="dole" value="2" name="tractor">
           <input type="radio" id="tele" value="3" name="tractor">
           <article class="card una">
               <h2 class="entypo-github">Label Uno</h2>
               <p>Three Labels for wach Input.<br/>
               One for go back,<br/>
               Another for go recover it.</p>
               <ul class="footer">
                   <li class="entypo-dribbble"></li>
                   <li class="entypo-book-open"></li>
                   <li class="entypo-language"></li>
               </ul>
               
               <label for="dole" class="entypo-left-open otra"></label>
               <label for="tele" class="entypo-right-open otra"></label>
               <label for="uno" class="entypo-arrows-ccw afin"></label>
               
           </article>
           
           <article class="card dos">
               <h2 class="entypo-network">Label Dos</h2>
               <p>In the Land of the Good Taste<br/>
                  where the Performers dwell.<br/>
                  One Input to rule them all,<br/>
                  One Input to find them.</p>
                  
                  
                  <ul class="footer">
                      <li class="entypo-network"></li>
                      <li class="entypo-qq"></li>
                      <li class="entypo-picasa"></li>
                  </ul>
                
                <label for="tele" class="entypo-left-bold otra"></label>
                <label for="uno" class="entypo-right-bold otra"></label>
                <label for="dole" class="entypo-arrows-ccw afin"></label>
                   
                   
               
               
           </article>
           
           <article class="card tres">
               <h2 class="entypo-light-down">Label Tres</h2>
               <p>Ine Input to bring them all<br/>
               and in the Html bind them<br/>
               In the Land of Styles<br/>
               where the Css is the King.</p>
               
               <ul class="footer">
                   <li class="entypo-ccw"></li>
                   <li class="entypo-arrows-ccw"></li>
                   <li class="entypo-cw"></li>
               </ul>
               
               <label for="uno" class="entypo-left-bold otra"></label>
               <label for="dole" class="entypo-right-bold otra"></label>
               <label for="tele" class="entypo-arrows-ccw afin"></label>
               
           </article>
           
           
           
       </section>
       
       <h1><a href="http://ksesocss.blogspot.com/2013/11/slide-Css-multiples-labels.html">Gallery pre/next pure Css. No Links</a></h1>
       <p>Who said that every input must have only one label?</p>
       
       
       
   </div>
   
    
</body>
</html>